function printErrorsinFields(errores,form){
    $('#'+form).find( '[name]' ).each( function( i , v ){
        let input = $( this ), // resolves to current input element.
            name = input.attr( 'name' ),
            value = input.val();
            if(errores[name]!=undefined){
                $('#'+name+'_spam').text(errores[name]);
                $('#'+name).css('border-color','#a94442')
            }
            else{
                $('#'+name+'_spam').text('')
                $('#'+name).css('border-color','')
            }
            /*$.each(errores, function( key, value ) {
                $('#'+key+'_spam').text(value);
                $('#'+key).css('border-color','#a94442')
            });*/

     });
}
function ClearformModalclose(form){
    $('#'+form).find( '[name]' ).each( function( i , v ){
        let input = $( this ), // resolves to current input element.
            name = input.attr( 'name' ),
            value = input.val();
            $('#'+name+'_spam').text('')
            $('#'+name).css('border-color','')
     });
}
function LoadTableGeneral(table_name,url,campostable){
    table_name.DataTable().destroy();
    let table=table_name.DataTable({
        processing: true,
        serverSide: false,
        responsive: true,
        searching: true,
        ordering: false,
        ajax: {
            type: 'get',
            dataType: 'json',
            url: url,
            async: false,
            dataSrc: "",
            //data: { "sp": sp_lista, "parameters": parametros, "pagination": false },
            /*complete: function(data) {
            }*/
        },
        language: {
            url: BASE_URL + 'js/plugin/datatables/Spanish.txt'
        },
        columns: campostable,
        scrollX: true,
        paging: true,
        pageLength: 10,
        lengthChange: false,
        dom: 'Bfrtip',
        buttons: [
            //'copy',  'print',            
            {
                extend: 'excelHtml5',
                text: /*'Excel',//*/ '<img src="' + BASE_URL + 'img/export_excel.png" />',
                titleAttr: 'Excel',
                customize: function(xlsx) {

                    var sheet = xlsx.xl.worksheets['sheet1.xml'];

                    $('row', sheet).each(function() {
                        fila = $(this).attr("r");
                        if (fila == 1) {
                            $(this).children().attr('s', '47');
                        } else {
                            if (fila % 2) {
                                $(this).children().attr('s', '33');
                            } else {
                                $(this).children().attr('s', '28');
                            }
                        }
                    });
                }
            }
        ],
        bDestroy: true,
        autoWidth   : false,
        //columnDefs: params.columnDefs,
        /*order: [
            [1, 'asc']
        ]*/
    });
}