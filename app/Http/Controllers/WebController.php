<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class WebController extends Controller{
    public function __construct(){
    
    }
    public function contacto(){
        return view('web.contacto');
    }
    public function nosotros(){
        return view('web.nosotros');
    }
    public function misionvision(){
        return view('web.nosotros');
    }
    public function servicios(){
        return view('web.servicios');
    }
    public function valores(){
        return view('web.valores');
    }
    public function cultura(){
        return view('web.cultura');
    }
    public function politicas(){
        return view('web.politica');
    }
}